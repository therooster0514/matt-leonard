﻿// Skeleton generated by Hyland Unity Editor on 11/5/2020 8:22:59 PM
//*******************************************************************
// Script Name: Template
// Script Purpose: Purpose goes here
// Script Scope: Workflow based script called by a timer or ad-hoc task
// Script Author: Matt Leonard
// Script Create Date: 2020-11-05
//*******************************************************************
//*******************************************************************
// Version Date: 2020-11-05
// Version Author: Matt Leonard
// Version Notes: Initial implementation
//*******************************************************************
namespace UnitySchedulerScriptSkeleton
{
    using System;
    using System.Text;
    using Hyland.Unity;
    using Hyland.Unity.CodeAnalysis;


    /// <summary>
    /// UnitySchedulerScriptSkeleton
    /// </summary>
    public class UnitySchedulerScriptSkeleton : Hyland.Unity.ISchedulableScript
    {

        #region ISchedulableScript
        /// <summary>
        /// Implementation of <see cref="ISchedulableScript.OnExecute" />.
        /// <seealso cref="ISchedulableScript" />
        /// </summary>
        /// <param name="app"></param>
        /// <param name="args"></param>
        public void OnExecute(Hyland.Unity.Application app, Hyland.Unity.SchedulableEventArgs args)
        {
            // Add Code Here
        }
        #endregion
    }
}
