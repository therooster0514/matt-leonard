﻿using System;
using System.Collections.Generic;
using Hyland.Unity;

namespace UnityHelper
{
    /// <summary>
    /// The UnityHelper library is designed to increase the productivity of developers by providing them with basic methods commonly used in Unity Scripting. 
    /// The current version of the Helper Library includes around 20 methods which are frequently used.
    /// To use the UnityHelper library, create an instance of the class and call the appropriate method.
    /// </summary>
    /// <example>The following is typical usage of the UnityHelper Library. <b>Please note</b> that a single KeywordModifier is created and passed to each UnityHelper method. This improves the performance of the Unity Script.
    /// <code> // Create a UnityHelper
    /// UnityHelper helper = new UnityHelper(app);
    /// 
    /// // Create a KeywordModifier for the document
    /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
    ///
    /// // Add/Update Keywords on the document
    /// helper.AddOrUpdateStandaloneKeyword(new StandaloneKeyword("Account #", "2"), args.Document, kMod);
    /// helper.AddStandaloneKeyword(new StandaloneKeyword("Description", "New Description"), kMod);
    /// helper.AddStandaloneKeyword(new StandaloneKeyword("Description", "New Description 2"), kMod);
    ///	 
    /// // Apply the Keyword Changes
    /// kMod.ApplyChanges();
    /// </code>
    /// </example>
    public class UnityHelper
    {
        private const string Version = "1.0";

        #region Variable(s)

        // Variable for Hyland.Application object 
        private Application _oApp = null;

        // Variable for class name 
        private string _sClassName = string.Empty;

        #endregion

        #region Constructor(s)

        /// <summary>
        /// Constructor used to instantiate a UnityHelper object.
        /// 
        /// </summary>
        /// <example>The following shows how to create an instance of the UnityHelper:
        /// <code> UnityHelper helper = new UnityHelper(app);
        /// </code>
        /// The following is a full example that includes making multiple keyword updates. <b>Please note</b> that a single KeywordModifier is created and passed to each UnityHelper method. This improves the performance of the Unity Script.
        /// <code> // Create a UnityHelper
        /// UnityHelper helper = new UnityHelper(app);
        /// 
        /// // Create a KeywordModifier for the document
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        ///
        /// // Add/Update Keywords on the document
        /// helper.AddOrUpdateStandaloneKeyword(new StandaloneKeyword("Account #", "2"), args.Document, kMod);
		/// helper.AddStandaloneKeyword(new StandaloneKeyword("Description", "New Description"), kMod);
		/// helper.AddStandaloneKeyword(new StandaloneKeyword("Description", "New Description 2"), kMod);
		///	 
		/// // Apply the Keyword Changes
		/// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_oApp">The OnBase Application Object.</param>
        public UnityHelper(Application a_oApp)
        {
            _oApp = a_oApp;
            _sClassName = this.GetType().Name;
        }

        #endregion

        #region Generic GetKeywordValue Method(s)

        /// <summary>
        /// Templatized method used to retrieve a Keyword value of a Standalone Keyword. This method assumes that the Keyword only has one instance.
        /// </summary>
        /// <example>The following example retrieves the value of the <b>Case Number</b> Keyword, which is an <b>Alphanumeric</b> Keyword:
        /// <code> string sCaseNumber = helper.GetKeywordValue&lt;string&gt;("Case Number", args.Document);
        /// </code>
        /// The following example retrieves the value of the <b>Submitted Date</b> Keyword, which is a <b>Date</b> Keyword:
        /// <code> DateTime sCaseNumber = helper.GetKeywordValue&lt;string&gt;("Submitted Date", args.Document);
        /// </code>
        /// </example>
        /// <typeparam name="T"><para>The .NET Type of the Keyword. The following lists Keyword Types and the corresponding .NET Type:<br/>
        /// Numeric9 = long<br/>
        /// Numeric20 = decimal<br/>
        /// Date = DateTime<br/>
        /// Date &amp; Time = DateTime<br/>
        /// Currency = decimal<br/>
        /// Floating Point = double<br/>
        /// Alphanumeric = string<br/><br/>
        /// An exception is thrown if the Type does not match the correct Type of the Keyword.</para></typeparam>
        /// <param name="a_sKeywordName">The Name of the Keyword Type to be returned. An exception will be thrown if the Keyword Name is not found.</param>
        /// <param name="a_oDoc">The OnBase Document to return the Keyword value from.</param>  
        /// <returns>The value (of Type T) of the keyword. If multiple instances of the keyword exist, only the first will be returned.</returns>
        public T GetKeywordValue<T>(string a_sKeywordName, Document a_oDoc)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            T oKeywordValue = default(T);

            try
            {
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Type<T>:'{typeof(T).ToString()}', Keyword Name:'{a_sKeywordName}', Doc Handle:'{a_oDoc.ID}'");

                // Get KeywordType object
                KeywordType keywordType = _oApp.Core.KeywordTypes.Find(a_sKeywordName);

                // Check if KeywordType object exists
                if (keywordType != null)
                {
                    _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"KeywordType Found: {keywordType.Name}");

                    // Retrieve the KeywordRecord object that contains the keyword 
                    KeywordRecord keyRecord = a_oDoc.KeywordRecords.Find(keywordType);

                    // Check if KeywordRecord Exists
                    if (keyRecord != null)
                    {
                        Keyword keyword = keyRecord.Keywords.Find(keywordType);

                        // Check if Keyword Exists
                        if (keyword != null && !keyword.IsBlank)
                        {
                            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Keyword Value Found: {keyword.Value}");

                            try
                            {
                                // Cast an object to a specific type
                                oKeywordValue = (T)Convert.ChangeType(keyword.Value, typeof(T));
                            }
                            catch (Exception ex)
                            {
                                throw new Exception($"Invalid Type<T> specified: {typeof(T).ToString()}.", ex);
                            }
                        }
                    }
                    else
                    {
                        _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"No KeywordRecord found for {a_sKeywordName} Keyword.");
                    }
                }
                else
                {
                    throw new Exception($"Invalid KeywordType: {a_sKeywordName}.");
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");

            return oKeywordValue;
        }

        /// <summary>
        /// Templatized method used to retrieve multiple Keyword values for Standalone Keywords that may have multiple instances.
        /// </summary>
        /// <example>The following example retrieves all values of the <b>Case Number</b> Keyword, which is an <b>Alphanumeric</b> Keyword:
        /// <code> List&lt;string&gt; sCaseNumbers = helper.GetKeywordValues&lt;string&gt;("Case Number", args.Document);
        /// 
        /// // Loop through each of the values
        /// foreach(string sValue in sCaseNumbers)
        /// {
        ///     // Do something with the sValue
        /// }
        /// </code>
        /// </example>
        /// <typeparam name="T"><para>The .NET Type of the Keyword. The following lists Keyword Types and the corresponding .NET Type:<br/>
        /// Numeric9 = long<br/>
        /// Numeric20 = decimal<br/>
        /// Date = DateTime<br/>
        /// Date &amp; Time = DateTime<br/>
        /// Currency = decimal<br/>
        /// Floating Point = double<br/>
        /// Alphanumeric = string<br/><br/>
        /// An exception is thrown if the Type does not match the correct Type of the Keyword.</para></typeparam>
        /// <param name="a_sKeywordName">The Name of the Keyword Type to be returned. An exception will be thrown if the Keyword Name is not found.</param>
        /// <param name="a_oDoc">The OnBase Document to return the Keyword values from.</param>       
        /// <returns>Returns a list of Keyword values. If no instances of the Keyword are found, an empty List will be returned.</returns>
        public List<T> GetKeywordValues<T>(string a_sKeywordName, Document a_oDoc)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Type<T>:'{typeof(T).ToString()}', Keyword Name:'{a_sKeywordName}', Doc Handle:'{a_oDoc.ID}'");

            List<T> keywordValues = new List<T>();

            try
            {
                // Get KeywordType object
                KeywordType keywordType = _oApp.Core.KeywordTypes.Find(a_sKeywordName);

                // Check if KeywordType object exists
                if (keywordType != null)
                {
                    _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"KeywordType found: {keywordType.Name}");

                    // Retrieve the KeywordRecord object that contains the keyword 
                    KeywordRecord keyRecord = a_oDoc.KeywordRecords.Find(keywordType);

                    // Check if KeywordRecord Exists
                    if (keyRecord != null)
                    {
                        KeywordList keywordList = keyRecord.Keywords.FindAll(keywordType);
                        foreach (Keyword keyword in keywordList)
                        {
                            // Check if Keyword Exists
                            if (!keyword.IsBlank)
                            {
                                try
                                {
                                    // Casting Keyword value to Generic type
                                    keywordValues.Add((T)Convert.ChangeType(keyword.Value, typeof(T)));
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception($"Invalid Type specified: {typeof(T).ToString()}.", ex);
                                }
                            }
                        }
                    }
                    else
                    {
                        _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"No KeywordRecord found for {a_sKeywordName} Keyword.");
                    }
                }
                else
                {
                    throw new Exception($"Invalid KeywordType: {a_sKeywordName}.");
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");

            return keywordValues;
        }

        #endregion

        #region GetMIKGGroupValue

        /// <summary>
        /// This method finds and returns specific instances from a Multi-Instance Keyword Group based on the search criteria specified (Search Keyword Name and Search Keyword Value). You must specify a Keyword Name and Value to find specific instances of the MIKG.
        /// </summary>
        /// <example>The following example retrieves all MIKG instances (Parties) where the <b>Party Type</b> Keyword is set to <b>Attorney</b>:
        /// <code> List&lt;MultiInstanceKeywordGroup&gt; parties = helper.GetSpecificMultiInstanceKeywordValue("Party", "Party Type", "Attorney", args.Document);
        /// 
        /// // Loop through each of parties that were found
        /// foreach(MultiInstanceKeywordGroup party in parties)
        /// {
        ///     // Loop through each of the StandaloneKeywords in the MIKG
        ///     foreach(StandaloneKeyword keyword in party)
        ///     {
        ///         // Do something with the party info
        ///         string sKeywordName = keyword.Name;
        ///         string sKeywordValue = keyword.Value;
        ///     }
        /// }
        /// </code>
        /// </example>
        /// <param name="a_sKeyTypeGroupName">The name of the Keyword Type Group. An exception will be thrown if the Keyword Type Group name is not found.</param>
        /// <param name="a_sSearchKWName">The Name of the Keyword used to search for specific instances. If this Keyword Type is not found, an exception will <u><b>not</b></u> be thrown. Instead, no instance will be returned.</param>
        /// <param name="a_sSearchKWValue">The value of the Search Keyword used to search against.</param>
        /// <param name="a_oDoc">The Document object to return the MIKG instance(s) from.</param>       
        /// <returns>A List of MultiInstanceKeywordGroup objects. If no instances are found, an empty List will be retuned.</returns>
        public List<MultiInstanceKeywordGroup> GetSpecificMultiInstanceKeywordValue(string a_sKeyTypeGroupName, string a_sSearchKWName, string a_sSearchKWValue, Document a_oDoc)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Keyword Group Name:'{a_sKeyTypeGroupName}', Search Keyword Name:'{a_sSearchKWName}', Search Keyword Value: '{a_sSearchKWValue}', Doc Handle:'{a_oDoc.ID}'");

            List<MultiInstanceKeywordGroup> lstMultiInstanceKeywordGroup = new List<MultiInstanceKeywordGroup>();

            try
            {
                KeywordRecordType keywordRecordType = _oApp.Core.KeywordRecordTypes.Find(a_sKeyTypeGroupName);

                if (keywordRecordType != null)
                {
                    // Retrieve existing keyword records
                    foreach (KeywordRecord keyRecord in a_oDoc.KeywordRecords.FindAll(keywordRecordType))
                    {
                        // For each instance of the MIKG, create a new MultiInstanceKeywordGroup object
                        MultiInstanceKeywordGroup oMultiInstanceKeywordGroup = new MultiInstanceKeywordGroup();
                        oMultiInstanceKeywordGroup.Name = keywordRecordType.Name;
                        bool bAddMIKG = false;

                        // Check if Primary Keyword Value matched
                        Keyword keywordExists = keyRecord.Keywords.Find(x => !x.IsBlank && x.Value.ToString().Equals(a_sSearchKWValue, StringComparison.InvariantCultureIgnoreCase)
                                                    && x.KeywordType.Name.Equals(a_sSearchKWName, StringComparison.InvariantCultureIgnoreCase));

                        // If a match was found
                        if (keywordExists != null)
                        {
                            // Set bAddMIKG: True 
                            bAddMIKG = true;

                            // Loop through all keywords in the instance
                            foreach (Keyword item in keyRecord.Keywords)
                            {
                                // If the keyword has a value (not null or empty)
                                if (!item.IsBlank)
                                {
                                    // If Match Found add 
                                    StandaloneKeyword oStandaloneKeyword = new StandaloneKeyword();
                                    oStandaloneKeyword.Name = item.KeywordType.Name;
                                    oStandaloneKeyword.Value = item.Value.ToString();

                                    // Add Standalone Keyword to collection
                                    oMultiInstanceKeywordGroup.MIKGKeywords.Add(oStandaloneKeyword);
                                }
                            }
                        }

                        // If at least one MIKG instance was found
                        if (bAddMIKG)
                        {
                            // Add the MIKG instance to the list to be returned
                            lstMultiInstanceKeywordGroup.Add(oMultiInstanceKeywordGroup);
                        }
                    }
                }
                else
                {
                    throw new Exception($"Invalid KeywordRecordType: {a_sKeyTypeGroupName}.");
                }

            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");

            return lstMultiInstanceKeywordGroup;
        }

        /// <summary>
        /// This method returns all instances of a Multi-Instance Keyword Group.
        /// </summary>
        /// <example>The following example retrieves all MIKG instances (Parties) on a Document:
        /// <code> List&lt;MultiInstanceKeywordGroup&gt; parties = helper.GetAllMultiInstanceKeywordValues("Party", args.Document);
        /// 
        /// // Loop through each of parties on the document
        /// foreach(MultiInstanceKeywordGroup party in parties)
        /// {
        ///     // Loop through each of the StandaloneKeywords in the MIKG
        ///     foreach(StandaloneKeyword keyword in party)
        ///     {
        ///         // Do something with the party info
        ///         string sKeywordName = keyword.Name;
        ///         string sKeywordValue = keyword.Value;
        ///     }
        /// }
        /// </code>
        /// </example>
        /// <param name="a_sKeyTypeGroupName">The name of the Keyword Type Group. An exception will be thrown if the Keyword Type Group name does not exist.</param>
        /// <param name="a_oDoc">The Document object to return the MIKG instance(s) from.</param>
        /// <returns>A List of MultiInstanceKeywordGroup objects. If no instances are found, an empty List will be retuned.</returns>
        public List<MultiInstanceKeywordGroup> GetAllMultiInstanceKeywordValues(string a_sKeyTypeGroupName, Document a_oDoc)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Keyword Group Name:'{a_sKeyTypeGroupName}', Doc Handle:'{a_oDoc.ID}'");

            List<MultiInstanceKeywordGroup> lstMultiInstanceKeywordGroup = new List<MultiInstanceKeywordGroup>();

            try
            {
                // Find the Keyword Record Type that matches the Keyword Type Group name that was passed in
                KeywordRecordType keywordRecordType = _oApp.Core.KeywordRecordTypes.Find(a_sKeyTypeGroupName);

                if (keywordRecordType != null)
                {
                    // Retrieve existing keyword records  
                    foreach (KeywordRecord keyRecord in a_oDoc.KeywordRecords.FindAll(keywordRecordType))
                    {
                        // For each instance of the MIKG, create a new MultiInstanceKeywordGroup object
                        MultiInstanceKeywordGroup oMultiInstanceKeywordGroup = new MultiInstanceKeywordGroup();
                        oMultiInstanceKeywordGroup.Name = keywordRecordType.Name;
                        bool bAddMIKG = false;

                        // Loop through all keywords in the instance
                        foreach (Keyword item in keyRecord.Keywords)
                        {
                            // If the keyword has a value (not null or empty)
                            if (!item.IsBlank)
                            {
                                // Create a StandaloneKeyword instance and add it to the MultiInstanceKeywordGroup object
                                StandaloneKeyword oStandaloneKeyword = new StandaloneKeyword();
                                oStandaloneKeyword.Name = item.KeywordType.Name;
                                oStandaloneKeyword.Value = item.Value.ToString();

                                // Add Standalone Keyword to collection
                                oMultiInstanceKeywordGroup.MIKGKeywords.Add(oStandaloneKeyword);

                                bAddMIKG = true;
                            }
                        }

                        // If at least one MIKG instance was found
                        if (bAddMIKG)
                        {
                            // Add the MIKG instance to the list to be returned
                            lstMultiInstanceKeywordGroup.Add(oMultiInstanceKeywordGroup);
                        }
                    }
                }
                else
                {
                    throw new Exception($"Invalid KeywordRecordType: {a_sKeyTypeGroupName}.");
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");

            return lstMultiInstanceKeywordGroup;
        }

        #endregion

        #region AddKeyword/Multi-Instance

        /// <summary>
        /// Add or Update a Keyword on a Document. If an instance of the Keyword already exists on the document, it will be updated. If it does not exist, then it will be added.
        /// </summary>
        /// <example> The following example updates the <b>Case Number</b> Keyword on the Document. If the Case Number keyword doesn't exist, it is added:
        /// <code> //Create a KeywordModifier for the document
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        /// 
        /// // Create a StandaloneKeyword
        /// StandaloneKeyword kw = new StandaloneKeyword("Case Number", "AB-123456-19");
        /// 
        /// // Add/Update the Keyword on the document
        /// helper.AddOrUpdateStandaloneKeyword(kw, args.Document, kMod);
        /// 
        /// // Apply the Keyword Changes
        /// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_oStandaloneKeyword">A StandaloneKeyword object to be added/updated. If the Keyword Name doesn't exist, an exception will be thrown.</param>
        /// <param name="a_oDoc">A Document object to add/update the Keyword on.</param>
        /// <param name="a_oKeyModifier">A KeywordModifier that will be used to add or update the Keyword. It is the responsibility of the developer to apply changes to the KeywordModifier after calling this method.</param>
        public void AddOrUpdateStandaloneKeyword(StandaloneKeyword a_oStandaloneKeyword, Document a_oDoc, KeywordModifier a_oKeyModifier)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Keyword Name:'{a_oStandaloneKeyword.Name}', Keyword Value:'{a_oStandaloneKeyword.Value}', Doc Handle:'{a_oDoc.ID}'");

            string skeywordType = a_oStandaloneKeyword.Name;
            string skeywordValue = a_oStandaloneKeyword.Value.Trim();

            try
            {
                // Make sure the StandaloneKeyword passed in has a value
                if (!string.IsNullOrEmpty(skeywordValue))
                {
                    // Find the Keyword Type from the Name of the StandaloneKeyword that was passed in
                    KeywordType keywordType = _oApp.Core.KeywordTypes.Find(skeywordType);

                    if (keywordType != null)
                    {
                        // Get the Keyword Value to validate
                        string sOldValue = this.GetKeywordValue<string>(skeywordType, a_oDoc);

                        // If the keyword doesn't already exist...
                        if (string.IsNullOrEmpty(sOldValue))
                        {
                            // Create Keyword Object
                            Keyword keyword = this.CreateKeyword(keywordType, skeywordValue);

                            // Add the keyword to the Keyword Modifier
                            a_oKeyModifier.AddKeyword(keyword);
                        }
                        else // If the keyword already has a value...
                        {
                            // Update the value
                            this.UpdateStandaloneKeyword(a_oStandaloneKeyword, a_oDoc, a_oKeyModifier);
                        }
                    }
                    else
                    {
                        // If the Keyword Type wasn't found, throw an exception
                        throw new Exception($"Invalid Keyword Type specified: {skeywordType}");
                    }
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        /// <summary>
        /// Add multiple Standalone Keywords to a Document.
        /// </summary>
        /// <example> The following example adds multiple keywords to a Document:
        /// <code> //Create a KeywordModifier for the document
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        /// 
        /// // Create a List of StandaloneKeywords
        /// List&lt;StandaloneKeyword&gt; kws = new List&lt;StandaloneKeyword&gt;();
        /// 
        /// // Add StandaloneKeywords to the List
        /// kws.Add(new StandaloneKeyword("Case Number", "AB-123456-19");
        /// kws.Add(new StandaloneKeyword("Case Title", "Snow vs White Walkers");
        /// kws.Add(new StandaloneKeyword("Case Type", "AB");
        /// 
        /// // Add the Keywords on the document
        /// helper.AddStandaloneKeywordList(kws, kMod);
        /// 
        /// // Apply the Keyword Changes
        /// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_lstStandaloneKeyword">List of StandaloneKeyword Objects to be added to the document.</param>
        /// <param name="a_oKeyModifier">A KeywordModifier that will be used to add or update the Keyword. It is the responsibility of the developer to apply changes to the KeywordModifier after calling this method.</param>        
        public void AddStandaloneKeywordList(List<StandaloneKeyword> a_lstStandaloneKeyword, KeywordModifier a_oKeyModifier)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Total Keywords:'{a_lstStandaloneKeyword.Count}'");

            try
            {
                // Ensure the list is not null and has at least one instance
                if (a_lstStandaloneKeyword != null && a_lstStandaloneKeyword.Count > 0)
                {
                    // Loop through all of the StandaloneKeywords
                    foreach (StandaloneKeyword standaloneKeyword in a_lstStandaloneKeyword)
                    {
                        // Add the StandaloneKeyword to the Document
                        AddStandaloneKeyword(standaloneKeyword, a_oKeyModifier);
                    }
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        /// <summary>
        /// Adds a new Keyword to a Document. If the document already has an instance of that Keyword Type then a new instance will be added.
        /// </summary>
        /// <example> The following example adds a <b>Case Number</b> Keyword to a Document:
        /// <code> //Create a KeywordModifier for the document
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        /// 
        /// // Create a StandaloneKeyword
        /// StandaloneKeyword kw = new StandaloneKeyword("Case Number", "AB-123456-19");
        /// 
        /// // Add/Update the Keyword on the document
        /// helper.AddStandaloneKeyword(kw, kMod);
        /// 
        /// // Apply the Keyword Changes
        /// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_oStandaloneKeyword">A StandaloneKeyword object to be added.</param>
        /// <param name="a_oKeyModifier">A KeywordModifier that will be used to add or update the Keyword. The ApplyChanges method must be called on the KeywordModifier after calling this method.</param> 
        public void AddStandaloneKeyword(StandaloneKeyword a_oStandaloneKeyword, KeywordModifier a_oKeyModifier)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Keyword Name:'{a_oStandaloneKeyword.Name}', Keyword Value:'{a_oStandaloneKeyword.Value}'");

            string skeywordType = a_oStandaloneKeyword.Name;
            string skeywordValue = a_oStandaloneKeyword.Value.Trim();

            try
            {
                if (!string.IsNullOrEmpty(skeywordValue))
                {
                    // Find the Keyword Type
                    KeywordType keywordType = _oApp.Core.KeywordTypes.Find(skeywordType);

                    if (keywordType != null)
                    {
                        // Create Keyword Object
                        Keyword keyword = this.CreateKeyword(keywordType, skeywordValue);

                        // Add the keyword to the keyword modifier
                        a_oKeyModifier.AddKeyword(keyword);
                    }
                    else
                    {
                        // If the Keyword Type wasn't found, throw an exception
                        throw new Exception($"Invalid Keyword Type specified: {skeywordType}");
                    }
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        /// <summary>
        /// Adds a new Keyword to a Document. If the document already has an instance of that Keyword Type then a new instance will be added.
        /// This method is intended to be used when Reindexing a document rather than updating Keywords.
        /// </summary>
        /// <example> The following example Reindexes the document to a <b>PA - Brief</b> Document Type and adds a <b>Case Number</b> Keyword to the document:
        /// <code> // DocumentType Name to reindex to
        /// string sDTName = "PA - Brief";
        /// 
        /// // Find the Document Type to reindex to
        /// DocumentType dt = app.Core.DocumentTypes.Find(sDTName);
        /// 
        /// // If the DocumentType wasn't found, throw an exception
        /// if(dt == null)
        /// {
        ///     throw new Exception($"DocumentType of {sDTName} does not exist. Document ({args.Document.ID}) will not be re-indexed.");
        /// }
        /// 
        /// //Create a ReindexProperties object
        /// ReindexProperties props = app.Core.Storage.CreateReindexProperties(args.Document, dt);
        /// 
        /// // Create a StandaloneKeyword
        /// StandaloneKeyword kw = new StandaloneKeyword("Case Number", "AB-123456-19");
        /// 
        /// // Add the Keyword on the document
        /// helper.AddStandaloneKeyword(kw, props);
        /// 
        /// // Reindex the Document
        /// Document newDoc = app.Core.Storage.ReindexDocument(props);
        /// </code>
        /// </example>
        /// <param name="a_oStandaloneKeyword">A StandaloneKeyword object to be added to the document. If the Keyword Name does not exist, an exception will be thrown.</param>
        /// <param name="a_oReindexProperties">A ReindexProperties object that will be used to add the keyword.</param>
        public void AddStandaloneKeyword(StandaloneKeyword a_oStandaloneKeyword, ReindexProperties a_oReindexProperties)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Keyword Name:'{a_oStandaloneKeyword.Name}', Keyword Value:'{a_oStandaloneKeyword.Value}'");

            string skeywordType = a_oStandaloneKeyword.Name;
            string skeywordValue = a_oStandaloneKeyword.Value.Trim();

            try
            {
                if (!string.IsNullOrEmpty(skeywordValue))
                {
                    // Find the Keyword Type
                    KeywordType keywordType = _oApp.Core.KeywordTypes.Find(skeywordType);

                    if (keywordType != null)
                    {
                        // Create Keyword Object
                        Keyword keyword = this.CreateKeyword(keywordType, skeywordValue);

                        // Add the keyword to the keyword modifier.
                        a_oReindexProperties.AddKeyword(keyword);
                    }
                    else
                    {
                        // If the Keyword Type wasn't found, throw an exception
                        throw new Exception($"Invalid Keyword Type specified: {skeywordType}");
                    }
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        /// <summary>
        /// Adds multiple Multi-Instance Keywords to a Document. This method will not update any existing instances of the Multi-Instance Keyword Group if they exist.
        /// </summary>
        /// <example> The following example adds multiple instances of the <b>Party</b> MIKG to a Document (one for Bob Smith and one for Sue Smith):
        /// <code> //Create a KeywordModifier for the document
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        /// 
        /// // Create a List of MIKGs to add to the Document
        /// List&lt;MultiInstanceKeywordGroup&gt; mikgs = new List&lt;MultiInstanceKeywordGroup&gt;();
        /// 
        /// // Create a MultiInstanceKeywordGroup for Bob
        /// MultiInstanceKeywordGroup mikg = new MultiInstanceKeywordGroup();
        /// mikg.Name = "Party";
        /// 
        /// mikg.MIKGKeywords.Add(new StandaloneKeyword("First Name", "Bob"));
        /// mikg.MIKGKeywords.Add(new StandaloneKeyword("Last Name", "Smith"));
        /// 
        /// // Add the mikg to the list
        /// mikgs.Add(mikg);
        /// 
        /// // Create a MultiInstanceKeywordGroup for Sue
        /// MultiInstanceKeywordGroup mikg2 = new MultiInstanceKeywordGroup();
        /// mikg2.Name = "Party";
        /// 
        /// mikg2.MIKGKeywords.Add(new StandaloneKeyword("First Name", "Sue"));
        /// mikg2.MIKGKeywords.Add(new StandaloneKeyword("Last Name", "Smith"));
        /// 
        /// // Add the mikg to the list
        /// mikgs.Add(mikg2);
        /// 
        /// // Add a MultiInstanceKeywordGroup
        /// helper.AddMultiInstanceKeywordList(mikgs, kMod);
        /// 
        /// // Apply the Keyword Changes
        /// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_lstMultiInstanceKeywordGroup">A List of MultiInstanceKeywordGroup objects to be added to the Document.</param>
        /// <param name="a_oKeyModifier">A KeywordModifier that will be used to add or update the Keyword. The ApplyChanges method must be called on the KeywordModifier after calling this method.</param>   
        public void AddMultiInstanceKeywordList(List<MultiInstanceKeywordGroup> a_lstMultiInstanceKeywordGroup, KeywordModifier a_oKeyModifier)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Total Keyword Group Instances:'{a_lstMultiInstanceKeywordGroup.Count}'");

            try
            {
                // Ensure the list of MIKGs passed in is not null and has at least one instance
                if (a_lstMultiInstanceKeywordGroup != null && a_lstMultiInstanceKeywordGroup.Count > 0)
                {
                    // Loop through each of the instances and add the MIKG to the document
                    foreach (MultiInstanceKeywordGroup MIKGKeyword in a_lstMultiInstanceKeywordGroup)
                    {
                        AddMultiInstanceKeyword(MIKGKeyword, a_oKeyModifier);
                    }
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        /// <summary>
        /// Adds a Multi-Instance Keyword to a Document. This method will not update any existing instances of the Multi-Instance Keyword Group if they exist.
        /// </summary>
        /// <example> The following example adds an instance of the <b>Party</b> MIKG to the document:
        /// <code> //Create a KeywordModifier for the document
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        /// 
        /// // Create a MultiInstanceKeywordGroup
        /// MultiInstanceKeywordGroup mikg = new MultiInstanceKeywordGroup();
        /// mikg.Name = "Party";
        /// 
        /// mikg.MIKGKeywords.Add(new StandaloneKeyword("First Name", "Bob"));
        /// mikg.MIKGKeywords.Add(new StandaloneKeyword("Last Name", "Smith"));
        /// 
        /// // Add a MultiInstanceKeywordGroup
        /// helper.AddMultiInstanceKeyword(mikg, kMod);
        /// 
        /// // Apply the Keyword Changes
        /// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_oMultiInstanceKeywordGroup">A MultiInstanceKeywordGroup object to be added to the Document.</param>
        /// <param name="a_oKeyModifier">A KeywordModifier that will be used to add or update the Keyword. The ApplyChanges method must be called on the KeywordModifier after calling this method.</param>
        public void AddMultiInstanceKeyword(MultiInstanceKeywordGroup a_oMultiInstanceKeywordGroup, KeywordModifier a_oKeyModifier)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: MIKG Group Name:'{a_oMultiInstanceKeywordGroup.Name}'");

            try
            {
                string sMultiInstanceKeywordGroup = a_oMultiInstanceKeywordGroup.Name;

                // Ensure the MIKG passed in has a Name and at least one instance exists
                if (!string.IsNullOrEmpty(sMultiInstanceKeywordGroup) && a_oMultiInstanceKeywordGroup.MIKGKeywords.Count > 0)
                {
                    // Find the Keyword Record Type for the new keyword record
                    KeywordRecordType keywordRecordType = _oApp.Core.KeywordRecordTypes.Find(sMultiInstanceKeywordGroup);

                    if (keywordRecordType != null)
                    {
                        // Create a new EditableKeywordRecord
                        EditableKeywordRecord editableKeywordRecord = keywordRecordType.CreateEditableKeywordRecord();

                        // Loop through all MIKG Keywords
                        foreach (StandaloneKeyword stdKeyword in a_oMultiInstanceKeywordGroup.MIKGKeywords)
                        {
                            //a_App.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"{0}:  {1}", stdKeyword.Key, stdKeyword.Value));

                            // Add the Keyword to the EditableKeywordRecord
                            AddKeyword(stdKeyword.Name, stdKeyword.Value, keywordRecordType, editableKeywordRecord);
                        }

                        // Add the EditableKeywordRecord to the KeywordModifier
                        a_oKeyModifier.AddKeywordRecord(editableKeywordRecord);
                    }
                    else
                    {
                        // If the Keyword Type wasn't found, throw an exception
                        throw new Exception($"Invalid MIKG specified: {sMultiInstanceKeywordGroup}");
                    }
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        /// <summary>
        /// Adds an Individual Keyword for Multi-Instance Keyword Group.
        /// </summary>
        /// <example>The following example adds a new <b>Party</b> MIKG instance to the Document:
        /// <code> // Create a KeywordModifier
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        /// 
        /// string sMIKGName = "Party";
        /// 
        /// // Find the KeywordRecordType for the MIKG
        /// KeywordRecordType krt = app.Core.KeywordRecordTypes.Find(sMIKGName);
        /// 
        /// // If the KeywordRecordType wasn't found, throw an exception
        /// if(krt == null)
        /// {
        ///     throw new Exception($"KeywordRecordType of {sMIKGName} does not exist. Keywords will not be modified on Document ({args.Document.ID}).");
        /// }
        /// 
        /// // Create an EditableKeywordRecord
        /// EditableKeywordRecord ekr = krt.CreateEditableKeywordRecord();
        /// 
        /// // Add keywords to the EditableKeywordRecord
        /// helper.AddKeyword("First Name", "Bob", krt, ekr);
        /// helper.AddKeyword("Last Name", "Smith", krt, ekr);
        /// helper.AddKeyword("Party Type", "Attorney", krt, ekr);
        /// 
        /// // Add the MIKG to the Document
        /// kMod.AddKeywordRecord(ekr);
        /// 
        /// // Apply the Changes to the KeywordModifier
        /// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_sKeyType">The name of the Keyword to be added to the EditableKeywordRecord. If the Name of the Keyword doesn't exist, then an exception will be thrown.</param>
        /// <param name="a_sKeyValue">The value of the Keyword to be added to the EditableKeywordRecord.</param>
        /// <param name="a_oKeywordRecordType">The KeywordRecordType of the MIKG.</param>
        /// <param name="a_oEditableKeywordRecord">The EditableKeywordRecord object used to add Keywords to the MIKG.</param>     
        private void AddKeyword(string a_sKeyType, string a_sKeyValue, KeywordRecordType a_oKeywordRecordType, EditableKeywordRecord a_oEditableKeywordRecord)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Keyword Type:'{a_sKeyType}', Keyword Value:'{a_sKeyValue}', Keyword Record Type:'{a_oKeywordRecordType.Name}'");

            try
            {
                // Ensure the Keyword has a value
                if (!string.IsNullOrEmpty(a_sKeyValue.Trim()))
                {
                    // Find the Keyword Type
                    KeywordType keywordType = a_oKeywordRecordType.KeywordTypes.Find(a_sKeyType);

                    if (keywordType != null)
                    {
                        // Create Keyword Object
                        Keyword keyword = this.CreateKeyword(keywordType, a_sKeyValue);

                        // Add the keyword to the EditableKeywordRecord
                        a_oEditableKeywordRecord.AddKeyword(keyword);
                    }
                    else
                    {
                        // If the Keyword Type wasn't found, throw an exception
                        throw new Exception($"Invalid Keyword Type specified: {a_sKeyType}");
                    }
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        #endregion

        #region GetPageData

        /// <summary>
        /// Returns the PageData for a specific Document Rendition. 
        /// </summary>
        /// <example>The following example retrieves the PageData for the default revision of the latest rendition using the appropriate DataProvider based on the MimeType of the Rendition:
        /// <code> PageData pd = helper.GetPageData(args.Document.DefaultRenditionOfLatestRevision);
        /// </code>
        /// </example>
        /// <param name="a_oCurrentRendition">The Rendition to be returned.</param>        
        /// <param name="a_bReturnDefault"><b>Optional</b>. If no value is passed, <b>true</b> will be used. 
        /// Passing <b>true</b> will use the Default DataProvider. Passing <b>false</b> will determine the appropriate provider to use based on the Mime Type of the Rendition.</param>
        /// <returns>A Hyland.Unity.PageData object.</returns>
        public PageData GetPageData(Rendition a_oCurrentRendition, bool a_bReturnDefault = true)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Revision ID:'{a_oCurrentRendition.Revision.ID}', Use Default DataProvider:'{a_bReturnDefault}'");

            PageData pageData = null;

            try
            {
                // If the Default Provider was not specified
                if (!a_bReturnDefault)
                {
                    if (string.Equals(a_oCurrentRendition.FileType.MimeType, "image/tiff", StringComparison.OrdinalIgnoreCase)
                        || string.Equals(a_oCurrentRendition.FileType.MimeType, "image/jpg", StringComparison.OrdinalIgnoreCase))
                    {
                        ImageDataProvider imgProvider = _oApp.Core.Retrieval.Image;
                        pageData = imgProvider.GetDocument(a_oCurrentRendition);
                    }
                    else if (string.Equals(a_oCurrentRendition.FileType.MimeType, "text/xml", StringComparison.OrdinalIgnoreCase))
                    {
                        NativeDataProvider nativeProvider = _oApp.Core.Retrieval.Native;
                        pageData = nativeProvider.GetDocument(a_oCurrentRendition);
                    }
                    else if (string.Equals(a_oCurrentRendition.FileType.MimeType, "application/pdf", StringComparison.OrdinalIgnoreCase))
                    {
                        PDFDataProvider pdfProvider = _oApp.Core.Retrieval.PDF;
                        pageData = pdfProvider.GetDocument(a_oCurrentRendition);
                    }
                    else
                    {
                        DefaultDataProvider defaultProvider = _oApp.Core.Retrieval.Default;
                        pageData = defaultProvider.GetDocument(a_oCurrentRendition);
                    }
                }
                else
                {
                    // If using the default provider
                    DefaultDataProvider defaultProvider = _oApp.Core.Retrieval.Default;
                    pageData = defaultProvider.GetDocument(a_oCurrentRendition);
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");

            return pageData;
        }

        #endregion

        #region DeleteKeyword/Multi-Instance

        /// <summary>
        /// Deletes all instances of a Keyword from a Document.
        /// </summary>
        /// <example>The following example removes all instances of the <b>Case Number</b> Keyword from the Document:
        /// <code> //Create a KeywordModifier
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        /// 
        /// // Delete the Keyword on the document
        /// helper.DeleteStandaloneKeyword("Case Number", args.Document, kMod);
        /// 
        /// // Apply the Changes to the Document
        /// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_sKeyType">The name of the Keyword Type to be removed.</param>
        /// <param name="a_oDoc">The Document object to remove the Keyword from.</param>
        /// <param name="a_oKeyModifier">A KeywordModifier that will be used to add or update the Keyword. The ApplyChanges method must be called on the KeywordModifier after calling this method.</param>
        public void DeleteStandaloneKeyword(string a_sKeyType, Document a_oDoc, KeywordModifier a_oKeyModifier)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Keyword Type:'{a_sKeyType}', Document ID:'{a_oDoc.ID}'");

            try
            {
                if (!string.IsNullOrEmpty(a_sKeyType.Trim()))
                {
                    // Find the KeywordType
                    KeywordType keywordType = _oApp.Core.KeywordTypes.Find(a_sKeyType);

                    if (keywordType != null)
                    {
                        // Find keyword record that contains keyword to remove
                        KeywordRecord keyRecord = a_oDoc.KeywordRecords.Find(keywordType);

                        if (keyRecord != null)
                        {
                            // Retrieve Keyword to remove
                            foreach (Keyword keyword in keyRecord.Keywords.FindAll(keywordType))
                            {
                                // Remove the keyword
                                a_oKeyModifier.RemoveKeyword(keyword);
                            }
                        }
                        else
                        {
                            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"No KeywordRecord found for {a_sKeyType} keyword.");
                        }
                    }
                    else
                    {
                        // If the Keyword Type wasn't found, throw an exception
                        throw new Exception($"Invalid Keyword Type specified: {a_sKeyType}");
                    }
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        /// <summary>
        /// Removes a Keyword from all instances of a Multi-Instance Keyword Group.
        /// </summary>
        /// <example>The following example removes all instances of the <b>First Name</b> Keyword from all <b>Party</b> MIKGs:
        /// <code> //Create a KeywordModifier
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        /// 
        /// // Delete the Keyword on the document
        /// helper.DeleteMultiInstanceKeyword("First Name", args.Document, kMod);
        /// 
        /// // Apply the Changes to the Document
        /// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_sKeyType">The name of the Keyword to be removed.</param>
        /// <param name="a_oDoc">The Document object to remove the keywords from.</param>
        /// <param name="a_oKeyModifier">A KeywordModifier that will be used to add or update the Keyword. The ApplyChanges method must be called on the KeywordModifier after calling this method.</param>
        public void DeleteMultiInstanceKeyword(string a_sKeyType, Document a_oDoc, KeywordModifier a_oKeyModifier)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Keyword Type:'{a_sKeyType}', Document ID:'{a_oDoc.ID}'");

            try
            {
                if (!string.IsNullOrEmpty(a_sKeyType.Trim()))
                {
                    // Find the keyword type (used to find the keyword record)
                    KeywordType keywordType = _oApp.Core.KeywordTypes.Find(a_sKeyType);

                    // Check if KeywordType Exists
                    if (keywordType != null)
                    {
                        // Retrieve existing multi instance keyword records that need to be modified 
                        foreach (KeywordRecord keyRecord in a_oDoc.KeywordRecords.FindAll(keywordType))
                        {
                            // Create an editable keyword record from existing record
                            EditableKeywordRecord editableKeywordRecord = keyRecord.CreateEditableKeywordRecord();

                            // Find the existing keyword(s) to be removed from the record
                            Keyword keyword = editableKeywordRecord.Keywords.Find(keywordType);

                            // Remove the keyword from the editable keyword record
                            editableKeywordRecord.RemoveKeyword(keyword);

                            // Update keyword record on KeywordModifier
                            a_oKeyModifier.UpdateKeywordRecord(editableKeywordRecord);
                        }
                    }
                    else
                    {
                        // If the Keyword Type wasn't found, throw an exception
                        throw new Exception($"Invalid Keyword Type specified: {a_sKeyType}");
                    }
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        /// <summary>
        /// Removes all instances of a Multi-Instance Keyword Group.
        /// </summary>
        /// <example>The following example removes all instances of the <b>Party</b> MIKG:
        /// <code> //Create a KeywordModifier
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        /// 
        /// // Delete the MIKG on the document
        /// helper.DeleteAllMultiInstanceKeywords("Party", args.Document, kMod);
        /// 
        /// // Apply the Changes to the Document
        /// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_sMIKGName">The name of the Multi-Instance Keyword Group.</param>
        /// <param name="a_oDoc">The Document object to remove the MIKGs from.</param>
        /// <param name="a_oKeywordModifier">A KeywordModifier that will be used to add or update the Keyword. The ApplyChanges method must be called on the KeywordModifier after calling this method.</param>
        public void DeleteAllMultiInstanceKeywords(string a_sMIKGName, Document a_oDoc, KeywordModifier a_oKeywordModifier)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: MIKG Name:'{a_sMIKGName}', Document ID:'{a_oDoc.ID}'");

            try
            {
                // Find the Keyword Record for the MIKG
                KeywordRecordType keywordRecordType = _oApp.Core.KeywordRecordTypes.Find(a_sMIKGName);

                if (keywordRecordType != null)
                {
                    // Retrieve existing multi instance keyword records that need to be modified 
                    foreach (KeywordRecord keyRecord in a_oDoc.KeywordRecords.FindAll(keywordRecordType))
                    {
                        // Create an EditableKeywordRecord
                        EditableKeywordRecord editableKeywordRecord = keyRecord.CreateEditableKeywordRecord();

                        // Loop through each of the Keywords in the MIKG
                        foreach (Keyword keyword in keyRecord.Keywords)
                        {
                            // Remove the Keyword from the EditableKeywordRecord
                            editableKeywordRecord.RemoveKeyword(keyword);
                        }

                        // Update the KeywordRecord in the KeywordModifier
                        a_oKeywordModifier.UpdateKeywordRecord(editableKeywordRecord);
                    }
                }
                else
                {
                    // If the MIKG wasn't found, throw an exception
                    throw new Exception($"Invalid MIKG specified: {a_sMIKGName}");
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        /// <summary>
        /// Removes a Keyword from a specific instance of a Multi-Instance Keyword Group. 
        /// </summary>
        /// <example>The following example removes instances of the <b>Party</b> MIKG that have a <b>Party Type</b> of <b>Attorney</b>:
        /// <code> //Create a KeywordModifier
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        /// 
        /// // Delete the MIKG on the document
        /// helper.DeleteSpecificMultiInstanceKeyword("Party", "Party Type", "Attorney", args.Document, kMod);
        /// 
        /// // Apply the Changes to the Document
        /// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_sMIKGName">The name of the Multi-Instance Keyword group to be searched.</param>
        /// <param name="a_sPrimaryKW">The Keyword Name used to search for a specific instance.</param>
        /// <param name="a_sPrimaryKWValue">The Keyword Value used to search for a specific instance.</param>
        /// <param name="a_oDoc">The Document object to remove the Keywords from.</param>
        /// <param name="a_oKeywordModifier">A KeywordModifier that will be used to add or update the Keyword. The ApplyChanges method must be called on the KeywordModifier after calling this method.</param>
        public void DeleteSpecificMultiInstanceKeyword(string a_sMIKGName, string a_sPrimaryKW, string a_sPrimaryKWValue, Document a_oDoc, KeywordModifier a_okeyModifier)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: MIKG Name:'{a_sMIKGName}', Search Keyword Type:'{a_sPrimaryKW}', Search Keyword Value:'{a_sPrimaryKWValue}', Document ID:'{a_oDoc.ID}'");

            string sKeyTypeGroup = a_sMIKGName;

            try
            {
                KeywordRecordType keywordRecordType = _oApp.Core.KeywordRecordTypes.Find(sKeyTypeGroup);

                if (keywordRecordType != null)
                {
                    // Retrieve existing keyword records that need to be modified 
                    foreach (KeywordRecord keyRecord in a_oDoc.KeywordRecords.FindAll(keywordRecordType))
                    {
                        bool bUpdateKeyModifier = false;

                        // Create an EditableKeywordRecord
                        EditableKeywordRecord editableKeywordRecord = keyRecord.CreateEditableKeywordRecord();

                        // Check if Primary Keyword Value matched
                        Keyword keywordExists = keyRecord.Keywords.Find(x => !x.IsBlank && x.Value.ToString().Equals(a_sPrimaryKWValue, StringComparison.InvariantCultureIgnoreCase)
                                                    && x.KeywordType.Name.Equals(a_sPrimaryKW, StringComparison.InvariantCultureIgnoreCase));

                        // If the Keyword was found in the MIKG
                        if (keywordExists != null)
                        {
                            // Loop through all of the keywords in that MKG
                            foreach (Keyword keyword in keyRecord.Keywords)
                            {
                                // Remove the keyword from the EditableKeywordRecord
                                editableKeywordRecord.RemoveKeyword(keyword);
                            }

                            bUpdateKeyModifier = true;
                        }

                        if (bUpdateKeyModifier)
                        {
                            // Update keyword record on KeywordModifier
                            a_okeyModifier.UpdateKeywordRecord(editableKeywordRecord);
                        }
                    }
                }
                else
                {
                    // If the MIKG wasn't found, throw an exception
                    throw new Exception($"Invalid MIKG specified: {a_sMIKGName}");
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }


        #endregion

        #region UpdateKeywords/Multi-Instance

        /// <summary>
        /// This method updates a Keyword on a Document. If an existing value does not exist for the Keyword, a new value will not be added.
        /// </summary>
        /// <example>The following example updates the <b>Case Number</b> Keyword on the Document:
        /// <code> // Create a new StandaloneKeyword to update to
        /// StandaloneKeyword updatedKeyword = new StandaloneKeyword("Case Number", "AA-123456-19");
        /// 
        /// // Create a KeywordModifier
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        /// 
        /// // Update the keyword
        /// helper.UpdateStandaloneKeyword(updatedKeyword, args.Document, kMod);
        /// 
        /// // Apply the changes to the KeywordModifier
        /// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_oStandaloneKeyword">The StandaloneKeyword to be updated to. An exception will be thrown if the Keyword doesn't exist.</param>
        /// <param name="a_oDoc">The Document object to be updated.</param>
        /// <param name="a_oKeyModifier">A KeywordModifier that will be used to add or update the Keyword. The ApplyChanges method must be called on the KeywordModifier after calling this method.</param>
        public void UpdateStandaloneKeyword(StandaloneKeyword a_oStandaloneKeyword, Document a_oDoc, KeywordModifier a_oKeyModifier)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Keyword Type:'{a_oStandaloneKeyword.Name}', Keyword Value:'{a_oStandaloneKeyword.Value}', Document ID:'{a_oDoc.ID}'");

            string sKeywordType = a_oStandaloneKeyword.Name;
            string sKeywordValue = a_oStandaloneKeyword.Value.Trim();

            try
            {
                // Ensure a Keyword Value was passed in
                if (!string.IsNullOrEmpty(sKeywordValue.Trim()))
                {
                    // Find the Keyword Type
                    KeywordType keywordType = _oApp.Core.KeywordTypes.Find(sKeywordType);

                    // Check if KeywordType != null
                    if (keywordType != null)
                    {
                        // Retrieve the keyword record that contains the keyword to be modified
                        KeywordRecord keyRecord = a_oDoc.KeywordRecords.Find(keywordType);

                        // Check If KeywordRecord Exists
                        if (keyRecord != null)
                        {
                            // Create new keyword for keyword type, to hold the updated value
                            Keyword newKeyword = this.CreateKeyword(keywordType, sKeywordValue);

                            // Retrieve keyword to update (could be multiple instances of the keyword)
                            foreach (Keyword keyword in keyRecord.Keywords.FindAll(keywordType))
                            {
                                // Update the keyword in the keyword modifier object
                                a_oKeyModifier.UpdateKeyword(keyword, newKeyword);
                            }
                        }
                    }
                    else
                    {
                        // If the Keyword Type wasn't found, throw an exception
                        throw new Exception($"Invalid Keyword Type specified: {sKeywordType}");
                    }
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        /// <summary>
        /// This method updates a Keyword on a Document. If an existing value does not exist for the Keyword, a new value will not be added.
        /// This method is intended to be used when ReindexProperties are available instead of a KeywordModifier.
        /// </summary>
        /// <example>The following example updates the <b>Case Number</b> Keyword on the Document:
        /// <code> // Create a new StandaloneKeyword to update to
        /// StandaloneKeyword updatedKeyword = new StandaloneKeyword("Case Number", "AA-123456-19");
        /// 
        /// // Update the keyword
        /// helper.UpdateStandaloneKeyword(updatedKeyword, args.Document, ReindexProperties);
        /// </code>
        /// </example>
        /// <param name="a_oStandaloneKeyword">The StandaloneKeyword to be updated to. An exception will be thrown if the Keyword doesn't exist.</param>
        /// <param name="a_oDoc">The Document object to be updated.</param>
        /// <param name="a_oReindexProperties">A ReindexProperties object that will be used to add the keyword.</param>
        public void UpdateStandaloneKeyword(StandaloneKeyword a_oStandaloneKeyword, Document a_oDoc, ReindexProperties a_oReindexProperties)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: Keyword Type:'{a_oStandaloneKeyword.Name}', Keyword Value:'{a_oStandaloneKeyword.Value}', Document ID:'{a_oDoc.ID}'");

            string sKeywordType = a_oStandaloneKeyword.Name;
            string sKeywordValue = a_oStandaloneKeyword.Value.Trim();

            try
            {
                // Ensure a Keyword Value was passed in
                if (!string.IsNullOrEmpty(sKeywordValue.Trim()))
                {
                    // Find the Keyword Type
                    KeywordType keywordType = _oApp.Core.KeywordTypes.Find(sKeywordType);

                    // Check if KeywordType != null
                    if (keywordType != null)
                    {
                        // Retrieve the keyword record that contains the keyword to be modified
                        KeywordRecord keyRecord = a_oDoc.KeywordRecords.Find(keywordType);

                        // Check If KeywordRecord Exists
                        if (keyRecord != null)
                        {
                            // Create new keyword for keyword type, to hold the updated value
                            Keyword newKeyword = this.CreateKeyword(keywordType, sKeywordValue);

                            // Retrieve keyword to update (could be multiple instances of the keyword)
                            foreach (Keyword keyword in keyRecord.Keywords.FindAll(keywordType))
                            {
                                // Update the keyword in the keyword modifier object
                                a_oReindexProperties.UpdateKeyword(keyword, newKeyword);
                            }
                        }
                    }
                    else
                    {
                        // If the Keyword Type wasn't found, throw an exception
                        throw new Exception($"Invalid Keyword Type specified: {sKeywordType}");
                    }
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        /// <summary>
        /// This method updates a specific instance of a Multi-Instance Keyword Group based on the search criteria specified (Search Keyword Name and Value). 
        /// </summary>
        /// <example>The following example searches for and updates any <b>Party</b> MIKG instances where the <b>Party Type</b> equals <b>Attorney</b>:
        /// <code> //Create a new MultiInstanceKeywordGroup
        /// MultiInstanceKeywordGroup updateGroup = new MultiInstanceKeywordGroup();
        /// 
        /// // Set the Name of the Multi-Instance Keyword Group
        /// updateGroup.Name = "Party";
        /// 
        /// // Set the new Keywords of the MIKG
        /// updateGroup.MIKGKeywords.Add(new StandaloneKeyword("First Name", "Bob");
        /// updateGroup.MIKGKeywords.Add(new StandaloneKeyword("Last Name", "Smith");
        /// updateGroup.MIKGKeywords.Add(new StandaloneKeyword("Party Type", "Attorney");
        /// // Add any other keywords that exist in the MIKG
        /// 
        /// // Create a KeywordModifier for the Document
        /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
        /// 
        /// // Update the MIKG
        /// helper.UpdateSpecificMultiInstanceKeyword(updateGroup, "Party Type", "Attorney", args.Document, kMod);
        /// 
        /// // Apply the Changes to the KeywordModifier
        /// kMod.ApplyChanges();
        /// </code>
        /// </example>
        /// <param name="a_oMultiInstanceKeywordGroup">A MultiInstanceKeywordGroup object that specific instances will be updated to.</param>
        /// <param name="a_sSearchKWName">The Keyword Name used to search for a specific Multi-Instance Keyword Group instance to be updated.</param>
        /// <param name="a_sSearchKWValue">The Keyword Value used to search for a specific Multi-Instance Keyword Group instance to be updated.</param>
        /// <param name="a_oDoc">The Document object to be updated.</param>
        /// <param name="a_okeyModifier">A KeywordModifier that will be used to add or update the Keyword. The ApplyChanges method must be called on the KeywordModifier after calling this method.</param>
        public void UpdateSpecificMultiInstanceKeyword(MultiInstanceKeywordGroup a_oMultiInstanceKeywordGroup, string a_sSearchKWName, string a_sSearchKWValue, Document a_oDoc, KeywordModifier a_okeyModifier)
        {
            System.Reflection.MethodBase m = System.Reflection.MethodBase.GetCurrentMethod();
            string sMethodInfo = $"{m.ReflectedType.Name}.{m.Name}()";

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Entering {sMethodInfo} with parameters: MIKG Name:'{a_oMultiInstanceKeywordGroup.Name}', Search Keyword Type:'{a_sSearchKWName}', Search Keyword Value:'{a_sSearchKWValue}', Document ID:'{a_oDoc.ID}'");

            string sKeyTypeGroup = a_oMultiInstanceKeywordGroup.Name;

            try
            {
                KeywordRecordType keywordRecordType = _oApp.Core.KeywordRecordTypes.Find(sKeyTypeGroup);

                // Check if KeywordRecordType != null
                if (keywordRecordType != null)
                {
                    // Retrieve existing keyword records that need to be modified 
                    foreach (KeywordRecord keyRecord in a_oDoc.KeywordRecords.FindAll(keywordRecordType))
                    {
                        bool bUpdateKeyModifier = false;

                        // Create an EditableKeywordRecord from existing record
                        EditableKeywordRecord editableKeywordRecord = keyRecord.CreateEditableKeywordRecord();

                        // Check if Primary Keyword Value matched
                        Keyword keywordExists = keyRecord.Keywords.Find(x => !x.IsBlank && x.Value.ToString().Equals(a_sSearchKWValue, StringComparison.InvariantCultureIgnoreCase)
                                                    && x.KeywordType.Name.Equals(a_sSearchKWName, StringComparison.InvariantCultureIgnoreCase));

                        if (keywordExists != null)
                        {
                            // Loop through each Keyword in the MIKG that was passed in
                            foreach (StandaloneKeyword stdKeyword in a_oMultiInstanceKeywordGroup.MIKGKeywords)
                            {
                                // Find the Keyword Type from the MIKG passed in
                                KeywordType keywordType = _oApp.Core.KeywordTypes.Find(stdKeyword.Name);

                                // If the Keyword Type isn't found
                                if (keywordType == null)
                                {
                                    // Throw an exception
                                    throw new Exception($"{stdKeyword.Name} KeywordType not exist on {sKeyTypeGroup} MIKG.");
                                }

                                // Create new keyword for keyword type, to hold the updated value
                                Keyword newKeyword = this.CreateKeyword(keywordType, stdKeyword.Value);

                                // Find the keyword that need to be updated in the record
                                Keyword keyword = editableKeywordRecord.Keywords.Find(keywordType);

                                // Update the keyword in the editable keyword record object
                                editableKeywordRecord.UpdateKeyword(keyword, newKeyword);
                                bUpdateKeyModifier = true;
                            }
                        }

                        if (bUpdateKeyModifier)
                        {
                            // Update keyword record on KeywordModifier
                            a_okeyModifier.UpdateKeywordRecord(editableKeywordRecord);
                        }
                    }
                }
                else
                {
                    // If the Multi-Instance Keyword Group wasn't found
                    throw new Exception($"Invalid MultiInstance Keyword Group: {sKeyTypeGroup}");
                }
            }
            catch (Exception ex)
            {
                // If any exceptions occurred, write to Diagnositcs and rethrow
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, $"Exception occurred in {sMethodInfo}");
                _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, ex);
                throw ex;
            }

            _oApp.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, $"Exiting {sMethodInfo}");
        }

        #endregion

        #region	Create Keyword Helper

        /// <summary>
        /// This method creates a Keyword from a KeywordType and a Keyword Value.
        /// </summary>
        /// <param name="a_oKeywordType">The Hyland.Unity.KeywordType.</param>
        /// <param name="a_sValue">The string value of the Keyword.</param>
        /// <returns>A Hyland.Unity.Keyword object.</returns>
        private Keyword CreateKeyword(KeywordType a_oKeywordType, string a_sValue)
        {
            Keyword keyword = null;

            // Check the DataType of the Keyword, parse the value from the string passed in, and create the keyword
            switch (a_oKeywordType.DataType)
            {
                case KeywordDataType.Currency:
                case KeywordDataType.Numeric20:
                    decimal decVal = decimal.Parse(a_sValue);
                    keyword = a_oKeywordType.CreateKeyword(decVal);
                    break;
                case KeywordDataType.Date:
                case KeywordDataType.DateTime:
                    DateTime dtVal = DateTime.Parse(a_sValue);
                    keyword = a_oKeywordType.CreateKeyword(dtVal);
                    break;
                case KeywordDataType.FloatingPoint:
                    double dblVal = double.Parse(a_sValue);
                    keyword = a_oKeywordType.CreateKeyword(dblVal);
                    break;
                case KeywordDataType.Numeric9:
                    long lngVal = long.Parse(a_sValue);
                    keyword = a_oKeywordType.CreateKeyword(lngVal);
                    break;
                default:
                    keyword = a_oKeywordType.CreateKeyword(a_sValue);
                    break;
            }

            // return the keyword
            return keyword;
        }

        #endregion
    }

    #region Custom Classe(s)

    /// <summary>
    /// Class to store Standalone Keyword information (Name and Value).
    /// </summary> 
    /// <example> The following example shows how to update an existing StandaloneKeyword Keyword:
    /// <code> // Create a UnityHelper object 
    /// UnityHelper helper = new UnityHelper(app);
    /// 
    /// // Create a new StandaloneKeyword instance with the updated value
    /// StandaloneKeyword kw = new StandaloneKeyword("Case Number", "AB-123456-19");
    /// 
    /// // Create a KeywordModifier
    /// KeywordModifier kMod = args.Document.CreateKeywordModifier();
    /// 
    /// // Update the keyword
    /// helper.AddOrUpdateStandaloneKeyword(kw, args.Document, kMod);
    /// 
    /// // Apply the changes
    /// kMod.ApplyChanges();
    /// </code>
    /// </example>
    public class StandaloneKeyword
    {
        private string _sName = string.Empty;
        private string _sValue = string.Empty;

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <example> The following creates a StandaloneKeyword object using the Default Constructor and sets it's variables after creation.
        /// <code> StandaloneKeyword kw = new StandaloneKeyword();
        /// 
        /// kw.Name = "Case Number";
        /// kw.Value = "AB-123456-19";
        /// </code>
        /// </example>
        public StandaloneKeyword() { }

        /// <summary>
        /// Constructor to set Keyword Name and Value while creating the object.
        /// </summary>
        /// <example> The following creates a StandaloneKeyword object and sets it's variables at the same time.
        /// <code> StandaloneKeyword kw = new StandaloneKeyword("Case Number", "AB-123456-19");</code>
        /// </example>
        /// <param name="a_sName">The Keyword Name.</param>
        /// <param name="a_sValue">The Keyword Value.</param>
        public StandaloneKeyword(string a_sName, string a_sValue)
        {
            _sName = a_sName;
            _sValue = a_sValue;
        }

        /// <summary>
        /// The Keyword Name.
        /// </summary>
        public string Name
        {
            get { return _sName; }
            set { _sName = value; }
        }

        /// <summary>
        /// The Keyword Value.
        /// </summary>
        public string Value
        {
            get { return _sValue; }
            set { _sValue = value; }
        }
    }

    /// <summary>
    /// Class to store Multi-Instance Keyword(s).
    /// </summary>
    public class MultiInstanceKeywordGroup
    {
        private string _sName = string.Empty;
        private List<StandaloneKeyword> _oMIKGKeywords = null;

        public MultiInstanceKeywordGroup()
        {
            _oMIKGKeywords = new List<StandaloneKeyword>();
        }

        /// <summary>
        /// Multi-Instance Keyword Group Name
        /// </summary>
        public string Name
        {
            get { return _sName; }
            set { _sName = value; }
        }

        /// <summary>
        /// List of Standalone Keyword
        /// </summary>
        public List<StandaloneKeyword> MIKGKeywords
        {
            get { return _oMIKGKeywords; }
            set { _oMIKGKeywords = value; }
        }
    }

    /// <summary>
    /// Class to store Single-Instance Keyword(s).
    /// </summary>
    public class SingleInstanceKeywordGroup
    {
        private string _sName = string.Empty;
        private List<StandaloneKeyword> _oSIKGKeywords = null;

        public SingleInstanceKeywordGroup()
        {
            _oSIKGKeywords = new List<StandaloneKeyword>();
        }

        public string Name
        {
            get { return _sName; }
            set { _sName = value; }
        }

        public List<StandaloneKeyword> SIKGKeywords
        {
            get { return _oSIKGKeywords; }
            set { _oSIKGKeywords = value; }
        }
    }

    #endregion

}
