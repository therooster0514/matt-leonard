﻿// Skeleton generated by Hyland Unity Editor on 7/29/2020 10:29:00 AM
//*******************************************************************
// Script Name: Template
// Script Purpose: Purpose goes here
// Script Scope: Workflow based script called by a timer or ad-hoc task
// Script Author: Matt Leonard
// Script Create Date: 2020-07-29
//*******************************************************************
//*******************************************************************
// Version Date: 2020-07-29
// Version Author: Matt Leonard
// Version Notes: Initial implementation
//*******************************************************************
namespace UnitySchedulerImportFlatFileWorkflow
{
    using System;
    using System.Text;
    using System.IO;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using Hyland.Unity;
    using Hyland.Unity.CodeAnalysis;
    using System.Linq;


    class ImportConfiguration
    {
        public List<bool> configEnabled;
        public List<string> configMonitorFolder;
        public List<string> configFileSearch;
        public List<string> configKeyValuePairs;
        public List<string> configSeparator;
        public List<bool> configDelimiter;
        public List<string> configIgnoreLines;
        public List<long> configCreateDTID;



        protected static System.Configuration.Configuration setConfigFileAtRuntime(Application app, string sConfigFile)
        {
            app.Diagnostics.Write(string.Format("sConfigFile: {0}", sConfigFile));

            string runtimeconfigfile = sConfigFile;

            AppDomain.CurrentDomain.SetData("APP_CONFIG_FILE", sConfigFile);

            // Specify config settings at runtime.
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);

            return config;
        }
        public ImportConfiguration(Application app)
        {
            int i = 0;

            configEnabled = new List<bool>();
            configMonitorFolder = new List<string>();
            configFileSearch = new List<string>();
            configKeyValuePairs = new List<string>();
            configSeparator = new List<string>();
            configDelimiter = new List<bool>();
            configIgnoreLines = new List<string>();
            configCreateDTID = new List<long>();

            // Point to custom configuration file. THIS NEEDS TO BE UPDATED EACH TIME THE SCRIPT IS USED IN A NEW ENVIRONMENT.
            System.Configuration.Configuration config = setConfigFileAtRuntime(app, @"C:\Users\mleonard\source\repos\UnityWorkflowSkeletonEP3\UnitySchedulerImportFlatFile.config");

            // Get the collection of the section groups. 
            System.Configuration.ConfigurationSectionGroupCollection sectionGroups = config.SectionGroups;

            foreach (System.Configuration.ConfigurationSectionGroup sectionGroup in sectionGroups)
            {
                app.Diagnostics.Write(string.Format("sectionGroup.Name: {0}", sectionGroup.Name));

                if (sectionGroup.Name == "ProcessingGroups")
                {
                    foreach (System.Configuration.ConfigurationSection configurationSection in sectionGroup.Sections)
                    {
                        var section = System.Configuration.ConfigurationManager.GetSection(configurationSection.SectionInformation.SectionName) as NameValueCollection;

                        app.Diagnostics.Write(string.Format("configurationSection.SectionInformation.SectionName: {0}", configurationSection.SectionInformation.SectionName));

                        configEnabled.Add(bool.Parse(section["Enabled"]));
                        configMonitorFolder.Add(section["MonitorFolder"]);
                        configFileSearch.Add(section["FileSearch"]);
                        configKeyValuePairs.Add(section["KeyValuePairs"]);
                        configSeparator.Add(section["Separator"]);
                        configDelimiter.Add(bool.Parse(section["Delimiter"]));
                        configIgnoreLines.Add(section["IgnoreLines"]);
                        configCreateDTID.Add(long.Parse(section["CreateDTID"]));

                    }
                }
                i += 1;
            }
        }

    }
    /// <summary>
    /// UnityWorkflowSkeleton
    /// </summary>
    public class UnitySchedulerImportFlatFileWorkflow : Hyland.Unity.IWorkflowScript
    {
        // Create the property bag string variables to map them to back to workflow logic
        private const string propErrorMessage = "propErrorMessage";
        private const string propScriptName = "propScriptName";
        private const string propInput = "propInput";

        #region IWorkflowScript
        /// <summary>
        /// Implementation of <see cref="IWorkflowScript.OnWorkflowScriptExecute" />.
        /// <seealso cref="IWorkflowScript" />
        /// </summary>
        /// <param name="app"></param>
        /// <param name="args"></param>
        public void OnWorkflowScriptExecute(Hyland.Unity.Application app, Hyland.Unity.WorkflowEventArgs args)
        {
            // On failure writing of the file, tell workflow to use the False logic path
            args.ScriptResult = false;

            ImportConfiguration importConfig = new ImportConfiguration(app);

            int iGroupCount = importConfig.configMonitorFolder.Count;

            app.Diagnostics.Write(string.Format("iGroupCount: {0}", iGroupCount));

            for (int c = 0; c < iGroupCount; c++)
            {
                app.Diagnostics.Write(string.Format("c: {0}", c));
                // If the current configuration is disabled, move to the next one.
                if (!importConfig.configEnabled[c]) continue;

                // Setup OnBase form creation objects
                Storage storage = app.Core.Storage;

                // Assign document type 
                DocumentType documentType = app.Core.DocumentTypes.Find(importConfig.configCreateDTID[c]);

                app.Diagnostics.Write(string.Format("documentType.Name: {0}", documentType.Name));

                // Assign file format 
                FileType fileType = app.Core.FileTypes.Find("Electronic Form");

                try
                {
                    // Search the monitor directory for files that match the configuration
                    string[] arrFiles = Directory.GetFiles(importConfig.configMonitorFolder[c], importConfig.configFileSearch[c]);

                    app.Diagnostics.Write(string.Format("arrFiles[0]: {0}", arrFiles[0]));

                    // Iterate through the list of files that were found
                    foreach (string sFile in arrFiles)
                    {
                        app.Diagnostics.Write(string.Format("sFile: {0}", sFile));

                        // First, rename the file so it doesnt get picked up again
                        string sProcessingFile = string.Format("{0}.PROCESSING", Path.GetFileNameWithoutExtension(sFile));
                        File.Move(sFile, sProcessingFile);

                        // Read the current file and read each line
                        var lines = File.ReadLines(sProcessingFile);
                        foreach (var line in lines)
                        {
                            app.Diagnostics.Write(string.Format("line: {0}", line));

                            // Split the line into nodes based on the configuration
                            string[] arrRecords = line.Split(importConfig.configSeparator[c].ToCharArray());
                            string[] arrConfig = importConfig.configKeyValuePairs[c].Split(','); //0|Keyword1,1|Keyword2,2|Keyword3

                            List<string> listStorage = new List<string>();

                            for (int i = 0; i <= arrRecords.Length; i++)
                            {
                                app.Diagnostics.Write(string.Format("importConfig.configIgnoreLines[c]: {0}", importConfig.configIgnoreLines[c]));

                                if (importConfig.configIgnoreLines[c].Contains(","))
                                {
                                    string[] arrIgnoreLines = importConfig.configIgnoreLines[c].Split(',');
                                    // If found in the array, skip this line
                                    if (Array.Exists(arrIgnoreLines, element => element == i.ToString())) continue;
                                }
                                else if (!string.IsNullOrEmpty(importConfig.configIgnoreLines[c]))
                                {
                                    if (int.Parse(importConfig.configIgnoreLines[c]) == i) continue;

                                }



                                // Check if the current records needs to be stored as a KW
                                if (i != int.Parse(arrConfig[i].Split('|')[0])) continue;

                                string sValue = "";

                                if (importConfig.configDelimiter[c])
                                {
                                    sValue = arrRecords[i].Replace("\"", "");
                                }
                                else sValue = arrRecords[i];

                                app.Diagnostics.Write(string.Format("listStorage.Add: {0}", string.Format("{0}|{1}", arrConfig[i].Split('|')[1], sValue)));

                                // Add the keyword type name and keyword value to the storage list: Keyword Type Name|Value
                                listStorage.Add(string.Format("{0}|{1}", arrConfig[i].Split('|')[1], sValue));



                            }

                            // Create document properties object to store the keywords
                            StoreNewEFormProperties storeEFormProperties = storage.CreateStoreNewEFormProperties(documentType, fileType);

                            // Loop through each of the keyword to value pairs
                            foreach (string sValuePair in listStorage)
                            {

                                if (string.IsNullOrWhiteSpace(sValuePair.Split('|')[1]))
                                {
                                    app.Diagnostics.Write(string.Format("string.IsNullOrWhiteSpace(sValuePair.Split('|')[1]): {0}", string.IsNullOrWhiteSpace(sValuePair.Split('|')[1])));
                                    continue;

                                }

                                app.Diagnostics.Write(string.Format("storeEFormProperties.AddKeyword: {0}", sValuePair.Split('|')[0], sValuePair.Split('|')[1]));
                                // Store keywords
                                storeEFormProperties.AddKeyword(sValuePair.Split('|')[0], sValuePair.Split('|')[1]);

                            }

                            Document newDocument = app.Core.Storage.StoreNewEForm(storeEFormProperties);

                            app.Diagnostics.Write(string.Format("newDocument.ID: {0}", newDocument.ID));

                        }



                        // Lastly, rename the file when completed
                        string sProcessedFile = string.Format("{0}.PROCESSED", Path.GetFileNameWithoutExtension(sFile));
                        File.Move(sFile, sProcessedFile);


                    }


                }
                catch (UnityAPIException uae)
                {
                    // Obtain the line where the error happened
                    string lineNumber = uae.StackTrace.Substring(uae.StackTrace.LastIndexOf(" "));

                    // Write error information to the Diagnostic Console
                    app.Diagnostics.WriteIf(Hyland.Unity.Diagnostics.DiagnosticsLevel.Error, uae);
                }
                catch (Exception ex)
                {
                    // Obtain the line where the error happened
                    string lineNumber = ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(" "));

                    // Write error information to the Diagnostic Console
                    app.Diagnostics.WriteIf(Hyland.Unity.Diagnostics.DiagnosticsLevel.Error, ex);
                }


            }


        }
        #endregion
    }
}